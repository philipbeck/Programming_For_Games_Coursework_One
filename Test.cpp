#include <iostream>
#include <cmath>

#include "Quaternion.h"
#include "Vector3D.h"

using namespace std;

//method declarations
//VECTOR STUFF
void vectorTests();
//vector tests
bool testVectorStandardConstructor();
bool testVectorEquivalence();
bool testVectorNotEquivalence();
bool testVectorDefualtConstructor();
bool testVectorCopyConstructor();
bool testVectorSetters();
bool testVectorGetMagnitude();
bool testVectorAdditionOperator();
bool testVectorSubtractionOperator();
bool testVectorMultiplicationByScalar();
bool testVectorDivisionByScalar();
bool testVectorScalarProduct();
bool testVectorVectorProduct();
bool testVectorGetUnit();
bool testVectorFindOrthogonalUnit();

//QUATERNION STUFF
void quaternionTests();
//quaternion tests
bool testQuaternionStandardConstructor();
bool testQuaternionEquivalence();
bool testQuaternionNotEquivalence();
bool testQuaternionDefualtConstructor();
bool testQuaternionFromInt();
bool testQuaternionCopyConstructor();
bool testQuaternionSetters();
bool testQuaternionGetMagnitude();
bool testQuaternionAdditionOperator();
bool testQuaternionSubtractionOperator();
bool testQuaternionMultiplicationOperator();
bool testQuaternionEqualsOperator();

//MAIN METHOD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
int main(){
	//this line makes all the trues and falses print out as words rather than numbers
	cout.setf(ios::boolalpha);

	Quaternion q(1, 2, 3, 4);
	Vector3D v(1, 2, 3);
	
	//testing outputs here since a method can't really check if they've worked or not
	cout << "Testing cout for vector: " << v << "\n";
	cout << "Testing cout for quaternion: " << q << "\n\n";

	vectorTests();
	cout << endl;
	quaternionTests();

	char end;
	cin >> end;
	return 0;
}

//VECTOR TESTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void vectorTests() {
	cout << "====VECTOR TESTS====\n";
	cout << "Standard Constructor Test Passed = " << testVectorStandardConstructor() << "\n";
	cout << "Equivalence Opertor Test Passed = " << testVectorEquivalence() << "\n";
	cout << "Not Equivalence Opertor Test Passed = " << testVectorNotEquivalence() << "\n";
	cout << "Defualt Constructor Test Passed = " << testVectorDefualtConstructor() << "\n";
	cout << "Copy Constructor Test Passed = " << testVectorCopyConstructor() << "\n";
	cout << "Setters Test Passed = " << testVectorSetters() << "\n";
	cout << "Get Magnitude Test Passed = " << testVectorGetMagnitude() << "\n";
	cout << "Addition Operator Test Passed = " << testVectorAdditionOperator() << "\n";
	cout << "Subtraction Operator Test Passed = " << testVectorSubtractionOperator() << "\n";
	cout << "Subtraction Operator Test Passed = " << testVectorSubtractionOperator() << "\n";
	cout << "Multiplication by Scalar Test Passed = " << testVectorMultiplicationByScalar() << "\n";
	cout << "Division by Scalar Test Passed = " << testVectorDivisionByScalar() << "\n";
	cout << "Scalar Product Test Passed = " << testVectorScalarProduct() << "\n";
	cout << "Vector Product Test Passed = " << testVectorVectorProduct() << "\n";
	cout << "Get Unit Test Passed = " << testVectorGetUnit() << "\n";
	cout << "Get Orthoganal Unit Passed = " << testVectorFindOrthogonalUnit() << "\n";
}

bool testVectorStandardConstructor() {
	Vector3D v(1, 2, 3);
	if (v.getX() == 1 && v.getY() == 2 && v.getZ() == 3) {
		return true;
	}

	return false;
}
//from here onwards assume that the standard constructor already works

bool testVectorEquivalence() {
	Vector3D v1(1, 2, 3);
	Vector3D v2(1, 2, 3);
	Vector3D v3(4, 5, 6);

	if (v1 == v2 && !(v1 == v3)) {
		return true;
	}

	return false;
}
//after this assume that == already works

bool testVectorNotEquivalence() {
	Vector3D v1(1, 2, 3);
	Vector3D v2(1, 2, 3);
	Vector3D v3(4, 5, 6);

	if (v1 != v3 && !(v1 != v2)) {
		return true;
	}

	return false;
}

bool testVectorDefualtConstructor() {
	Vector3D v;

	if (v == Vector3D(0, 0, 0)) {
		return true;
	}

	return false;
}

bool testVectorCopyConstructor() {
	Vector3D v1(1, 2, 3);
	Vector3D v2(v1);
	if (v1 == v2){
		return true;
	}
	return false;
}

bool testVectorSetters(){
	Vector3D v;
	v.setX(1);
	v.setY(2);
	v.setZ(3);

	if (v == Vector3D(1, 2, 3)){
		return true;
	}

	return false;
}

bool testVectorGetMagnitude(){
	Vector3D v(1, 2, 3);
	//check that the magnitudes are very close to each other because maths
	//with floats isn't 100% acurate
	if (abs(v.getMagnitude() - sqrt(1 * 1 + 2 * 2 + 3 * 3)) < 0.00001) {
		return true;
	}

	return false;

}

bool testVectorAdditionOperator(){
	Vector3D v1(1, 2, 3);
	Vector3D v2(4, 5, 6);
	if (v1 + v2 == Vector3D(5, 7, 9)){
		return true;
	}

	return false;
}

bool testVectorSubtractionOperator(){
	Vector3D v1(1, 2, 3);
	Vector3D v2(3, 2, 1);
	if (v1 - v2 == Vector3D(-2, 0, 2)){
		return true;
	}

	return false;
}

bool testVectorMultiplicationByScalar(){
	Vector3D v(1, 2, 3);
	if (v * 3 == Vector3D(3, 6, 9)){
		return true;
	}

	return false;
}

bool testVectorDivisionByScalar(){
	Vector3D v(2, 4, 6);
	if (v / 2 == Vector3D(1, 2, 3)){
		return true;
	}
	
	return false;
}

bool testVectorScalarProduct(){
	Vector3D v1(1, 2, 3);
	Vector3D v2(4, 5, 6);
	//compares the Vector3D scalar product function output
	//to a scalar product worked out on the spot here
	if (v1 * v2 == ((1 * 4) + (2 * 5) + (3 * 6))){
		return true;
	}

	return false;
}

bool testVectorVectorProduct(){
	Vector3D v1(1, 2, 3);
	Vector3D v2(4, 5, 6);
	//vector product compared to correct answer worked out separately
	if (v1 % v2 == Vector3D(-3, 6, -3)){
		return true;
	}

	return false;
}

bool testVectorGetUnit(){
	Vector3D v(1, 2, 3);

	if (v.getUnit() == v / v.getMagnitude()){
		return true;
	}

	return false;
}

bool testVectorFindOrthogonalUnit(){
	Vector3D v1(1, 2, 3);
	Vector3D v2(4, 5, 6);
	if (v1.findOrthogonalUnit(v2) == (v1 % v2).getUnit()){
		return true;
	}

	return false;
} 
//QUATERNION TESTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void quaternionTests() {
	cout << "====QUATERNION TESTS====\n";
	cout << "Standard Constructor Test Passed = " << testQuaternionStandardConstructor() << "\n";
	cout << "Equivalence Opertor Test Passed = " << testQuaternionEquivalence() << "\n";
	cout << "Not Equivalence Opertor Test Passed = " << testQuaternionNotEquivalence() << "\n";
	cout << "Default Constructor Test Passed = " << testQuaternionDefualtConstructor() << "\n";
	cout << "Int to Quaternion Test Passed = " << testQuaternionFromInt() << "\n";
	cout << "Copy Constructor Test Passed = " << testQuaternionCopyConstructor() << "\n";
	cout << "Setters Test Passed = " << testQuaternionSetters() << "\n";
	cout << "Get Magnitude Test Passed = " << testQuaternionGetMagnitude() << "\n";
	cout << "Addition Operator Test Passed = " << testQuaternionAdditionOperator() << "\n";
	cout << "Subtraction Operator Test Passed = " << testQuaternionSubtractionOperator() << "\n";
	cout << "Multiplication Operator Test Passed = " << testQuaternionMultiplicationOperator() << "\n";
	cout << "Equals Operator Test Passed = " << testQuaternionEqualsOperator() << "\n";
}


bool testQuaternionStandardConstructor() {
	Quaternion q(1, 2, 3, 4);

	if (q.getR() == 1 && q.getI() == 2 && q.getJ() == 3 && q.getK() == 4) {
		return true;
	}

	return false;
}
//from here onwards assume that the standard constructor already works

bool testQuaternionEquivalence(){
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(1, 2, 3, 4);
	Quaternion q3(4, 3, 2, 1);
	//1 and 2 should be equal but 1 and 3 should not
	if (q1 == q2 && !(q1 == q3)){
		return true;
	}
	//otherwise return false
	return false;
}
//after this assume that == already works

bool testQuaternionNotEquivalence() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(1, 2, 3, 4);
	Quaternion q3(4, 3, 2, 1);
	//1 and 2 should be equal but 1 and 3 should not
	if (!(q1 != q2) && q1 != q3) {
		return true;
	}
	//otherwise return false
	return false;
}

bool testQuaternionDefualtConstructor() {
	//make new quaternion
	Quaternion q;
	//check that all values are equal to zero
	if (q != Quaternion(0, 0, 0, 0)) {
		return false;
	}

	return true;
}

bool testQuaternionFromInt() {
	//cast an int as a quaternion
	Quaternion q = 1;
	//check all the values are correct
	if (q != Quaternion(1, 0, 0, 0)) {
		return false;
	}

	return true;
}

bool testQuaternionCopyConstructor() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(q1);

	if (q1 == q2) {
		return true;
	}

	return false;
}

//tseting all of the setters in the same method
bool testQuaternionSetters() {
	Quaternion q;
	q.setR(1);
	q.setI(2);
	q.setJ(3);
	q.setK(4);

	if (q == Quaternion(1,2,3,4)) {
		return true;
	}

	return false;
}

bool testQuaternionGetMagnitude() {
	Quaternion q(1, 2, 3, 4);
	//check that the magnitudes are very close to each other because maths
	//with floats isn't 100% acurate
	if (abs(q.getMagnitude() - sqrt(1 * 1 + 2 * 2 + 3 * 3 + 4 * 4)) < 0.00001) {
		return true;
	}

	return false;
}

bool testQuaternionAdditionOperator() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(4, 3, 2, 1);
	if ((q1 + q2) == Quaternion(5, 5, 5, 5)) {
		return true;
	}

	return false;
}

bool testQuaternionSubtractionOperator() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(4, 3, 2, 1);
	if ((q1 - q2) == Quaternion(-3, -1, 1, 3)) {
		return true;
	}

	return false;
}

bool testQuaternionMultiplicationOperator() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2(4, 3, 2, 1);
	//compares the maths to an answer I worked out separately
	if ((q1 * q2) == Quaternion(-12, 6, 24, 12)) {
		return true;
	}

	return false;
}

bool testQuaternionEqualsOperator() {
	Quaternion q1(1, 2, 3, 4);
	Quaternion q2;
	//perform operation
	q2 = q1;
	if (q2 == q1) {
		return true;
	}

	return false;
}