#ifndef VECTOR_3D_H
#define VECTOR_3D_H

#include <ostream>

class Vector3D {
public:
	//constructors
	Vector3D();
	Vector3D(float x, float y, float z);
	//copy constructor
	Vector3D(Vector3D& other);
	//destructor?
	~Vector3D();
	//getters and setters
	inline float getX() const{
		return x;
	}
	inline void setX(float x){
		this->x = x;
	}

	inline float getY() const{
		return y;
	}
	inline void setY(float y){
		this->y = y;
	}

	inline float getZ() const{
		return z;
	}
	inline void setZ(float z){
		this->z = z;
	}
	//doing stuff
	float getMagnitude() const;
	//returns a unit vector pointing in the same direction
	Vector3D getUnit() const;

	//find orthogonal unit vector
	Vector3D findOrthogonalUnit(Vector3D vect) const;

	//operators
	//adding
	Vector3D operator+(const Vector3D& rhs) const;
	//subtraction
	Vector3D operator-(const Vector3D& rhs) const;
	//multiply by scalar
	Vector3D operator*(const float& rhs) const;
	//divide by a scalar
	Vector3D operator/(const float& rhs) const;
	//scalar
	float operator*(const Vector3D& rhs) const;
	//vector product
	Vector3D operator%(const Vector3D& rhs) const;
	//assignment operator
	Vector3D& operator=(const Vector3D& rhs);

	//equivelance
	bool operator==(const Vector3D& rhs);
	//not equal
	bool operator!=(const Vector3D& rhs);

private:
	float x, y, z;//co-ordinates should be floats
};

//override output opperator <<
std::ostream& operator<< (std::ostream& outStream, Vector3D vector);

#endif