#ifndef QUATERNION_H
#define QUATERNION_H

#include <ostream>

class Quaternion {
public:
	//constructors
	Quaternion();
	Quaternion(int r);//turns an int into a quaternion
	Quaternion(float r, float i, float j, float k);
	//copy constructor
	Quaternion(Quaternion& other);
	//destructor
	~Quaternion();
	//getters and setters
	inline float getR() const{
		return *pr;
	}
	inline void setR(float r){
		*pr = r;
	}

	inline float getI() const{
		return *pi;
	}
	inline void setI(float i){
		*pi = i;
	}

	inline float getJ() const{
		return *pj;
	}
	inline void setJ(float j){
		*pj = j;
	}


	inline float getK() const{
		return *pk;
	}
	inline void setK(float k){
		*pk = k;
	}

	//doing stuff
	float getMagnitude();
	//operators
	Quaternion operator+(const Quaternion& rhs) const;
	Quaternion operator-(const Quaternion& rhs) const;
	//horrible multiplication
	Quaternion operator*(const Quaternion& rhs) const;
	//assignment operator
	Quaternion& operator=(const Quaternion& rhs);
	//equivelance
	bool operator==(const Quaternion& rhs);
	//not equal
	bool operator!=(const Quaternion& rhs);
private:
	float* pr;//real part
	float* pi;
	float* pj;
	float* pk;
};

std::ostream& operator<< (std::ostream& outStream, const Quaternion& quaternion);

#endif