#include "Quaternion.h"

#include <cmath>

//constructors+++++++++++++++++++++++++++++++++++++++++++
Quaternion::Quaternion() {
	pr = new float(0);
	pi = new float(0);
	pj = new float(0);
	pk = new float(0);
}

Quaternion::Quaternion(int r) {
	pr = new float(r);
	pi = new float(0);
	pj = new float(0);
	pk = new float(0);
}


Quaternion::Quaternion(float r, float i, float j, float k) {
	pr = new float(r);
	pi = new float(i);
	pj = new float(j);
	pk = new float(k);
}

//copy constructor
Quaternion::Quaternion(Quaternion& other){
	pr = new float(other.getR());
	pi = new float(other.getI());
	pj = new float(other.getJ());
	pk = new float(other.getK());
}

//destructor+++++++++++++++++++++++++++++++++++++++++++
Quaternion::~Quaternion() {
	delete pr;
	delete pi;
	delete pj;
	delete pk;
}

//doing stuff+++++++++++++++++++++++++++++++++++++++++++
float Quaternion::getMagnitude() {
	return sqrt((*pr * *pr) + (*pi * *pi) + (*pj * *pj) + (*pk * *pk));
}
//operators+++++++++++++++++++++++++++++++++++++++++++

//addition
Quaternion Quaternion::operator+(const Quaternion& rhs) const{
	return Quaternion(*pr + rhs.getR(), *pi + rhs.getI(), *pj + rhs.getJ(), *pk + rhs.getK());
}

//subtraction
Quaternion Quaternion::operator-(const Quaternion& rhs) const{
	return Quaternion(*pr - rhs.getR(), *pi - rhs.getI(), *pj - rhs.getJ(), *pk - rhs.getK());
}

Quaternion Quaternion::operator*(const Quaternion& rhs) const{
	float r, i, j, k;
	//real part the brackets don't need to be there but it is easier to read
	r = (*pr * rhs.getR()) - (*pi * rhs.getI()) - (*pj * rhs.getJ()) - (*pk * rhs.getK());
	i = (*pr * rhs.getI()) + (*pi * rhs.getR()) + (*pj * rhs.getK()) - (*pk * rhs.getJ());
	j = (*pr * rhs.getJ()) - (*pi * rhs.getK()) + (*pj * rhs.getR()) + (*pk * rhs.getI());
	k = (*pr * rhs.getK()) + (*pi * rhs.getJ()) - (*pj * rhs.getI()) + (*pk * rhs.getR());
	return Quaternion(r, i, j, k);
}

//assignment operator
Quaternion& Quaternion::operator=(const Quaternion& rhs) {
	*pr = rhs.getR();
	*pi = rhs.getI();
	*pj = rhs.getJ();
	*pk = rhs.getK();
	return *this;
}

//equivalence
bool Quaternion::operator==(const Quaternion& rhs) {
	if (*pr == rhs.getR() && *pi == rhs.getI() && *pj == rhs.getJ() && *pk == rhs.getK()) {
		return true;
	}
	//if it didn't already return true then return false
	return false;
}

//not equal
bool Quaternion::operator!=(const Quaternion& rhs) {
	return !(*this == rhs);
}

//operator for cout
std::ostream& operator<< (std::ostream& outStream, const Quaternion& quaternion) {
	outStream << quaternion.getR() << " ";

	if (quaternion.getI() < 0)
		outStream << quaternion.getI() << "i ";
	else
		outStream << "+" << quaternion.getI() << "i ";

	if (quaternion.getJ() < 0)
		outStream << quaternion.getJ() << "j ";
	else
		outStream << "+" << quaternion.getJ() << "j ";

	if (quaternion.getK() < 0)
		outStream << quaternion.getK() << "k ";
	else
		outStream << "+" << quaternion.getK() << "k";
	return outStream;
}