#include "Vector3D.h"
#include <cmath>

//constructors++++++++++++++++++++++++++++++
Vector3D::Vector3D() {
	x = 0;
	y = 0;
	z = 0;
}

Vector3D::Vector3D(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
//copy constructor
Vector3D::Vector3D(Vector3D& other) {
	x = other.getX();
	y = other.getY();
	z = other.getZ();
}
//destructor++++++++++++++++++++++++++++++
Vector3D::~Vector3D() {}

//doing stuff+++++++++++++++++++++++++++++++++++++
float Vector3D::getMagnitude() const{
	return sqrt(x*x + y*y + z*z);//magnitude of a vector is worked out with trigonometry
}

Vector3D Vector3D::getUnit() const{
	return *this / getMagnitude();
}

//find orthogonal unit vector
Vector3D Vector3D::findOrthogonalUnit(Vector3D vect) const{
	Vector3D output(*this % vect);
	return output.getUnit();
}
//operators++++++++++++++++++++++++++++++++++++++

//adding
Vector3D Vector3D::operator+(const Vector3D& rhs) const {
	return Vector3D(x + rhs.getX(), y + rhs.getY(), z + rhs.getZ());
}

//subtracting
Vector3D Vector3D::operator-(const Vector3D& rhs) const {
	return Vector3D(x - rhs.getX(), y - rhs.getY(), z - rhs.getZ());
}

//multiply by a scalar
Vector3D Vector3D::operator*(const float& rhs) const {
	return Vector3D(x * rhs, y * rhs, z * rhs);
}

//divide by a scalar
Vector3D Vector3D::operator/(const float& rhs) const{
	return Vector3D(x / rhs, y / rhs, z / rhs);
}

//scalar product
float Vector3D::operator*(const Vector3D& rhs) const {
	return x * rhs.getX() + y * rhs.getY() + z * rhs.getZ();
}

//vector product
Vector3D Vector3D::operator%(const Vector3D& rhs) const {
	return Vector3D(y * rhs.getZ() - z * rhs.getY(), z * rhs.getX() - x * rhs.getZ(), x * rhs.getY() - y * rhs.getX());
}


Vector3D& Vector3D::operator=(const Vector3D& rhs) {
	x = rhs.getX();
	y = rhs.getY();
	z = rhs.getZ();
	return *this;
}

bool Vector3D::operator==(const Vector3D& rhs) {
	if (x == rhs.getX() && y == rhs.getY() && z == rhs.getZ()) {
		return true;
	}

	return false;
}

bool Vector3D::operator!=(const Vector3D& rhs) {
	return !(*this == rhs);
}

//overriding <<

std::ostream& operator<< (std::ostream& outStream, Vector3D vector) {
	outStream << "( " << vector.getX() << ", " << vector.getY() << ", " << vector.getZ() << " )";
	return outStream;
}